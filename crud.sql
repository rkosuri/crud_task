-- creating db
create database firstdatabase;

-- reading db
use firstdatabase;

-- updating db
create table firstdatabase(companyname VARCHAR(30), id int(25),city varchar(50), numberofemp int);

-- deleting db
 DROP DATABASE friends;
 
 
-- creating table 
create table friends(name VARCHAR(30), id int(25),college varchar(50), age int);

-- reading table
desc friends;

-- updating table
ALTER TABLE friends  ADD COLUMN phonenumber int;

-- deleting table
drop table friends;


-- creating record
INSERT INTO friends values ('ram',101,'GITAM',21),('harsha',102,'GITAM',21),('tinku',103,'GITAM',20),('pinku',104,'GITAM',21);

-- reading record
SELECT * FROM friends;

-- updating record
UPDATE friends SET name = 'ramana' WHERE id = 100;

-- deleting record
DELETE FROM friends WHERE id = 100;





